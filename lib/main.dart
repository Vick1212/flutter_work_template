import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:work/pages/page_login.dart';
import 'package:work/routes/app_pages.dart';
import 'package:work/theme/theme_constants.dart';
import 'package:work/theme/theme_manager.dart';
import 'controller/controller_home.dart';
import 'controller/controller_setting.dart';
import 'local_string.dart';

ThemeManager _themeManager = ThemeManager();

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);



  Get.put(ControllerHome());
  Get.put(ControllerSetting());

  final settingController = Get.put(ControllerSetting());
  Locale savedLocal = await settingController.getLocal();
  ThemeData savedTheme = await settingController.getTheme();

  runApp(
    GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: const MyApp(),
      locale: savedLocal,
      translations: LocaleString(),
      fallbackLocale: const Locale('en', 'US'),
      builder: EasyLoading.init(),
      theme: savedTheme,
      themeMode: _themeManager.themeMode,
      darkTheme: darkAppThemeData,
      getPages: AppPages.routes,
    ),
  );

  configLoading();

}

//如果要變更效果樣式，可參考  https://github.com/jogboms/flutter_spinkit#-showcase
void configLoading() {
  EasyLoading.instance
    ..displayDuration = const Duration(milliseconds: 2000)
    ..indicatorType = EasyLoadingIndicatorType.cubeGrid
    ..loadingStyle = EasyLoadingStyle.dark
    ..indicatorSize = 45.0
    ..radius = 10.0
    ..progressColor = Colors.yellow
    ..backgroundColor = Colors.green
    ..indicatorColor = Colors.yellow
    ..textColor = Colors.yellow
    ..maskColor = Colors.blue.withOpacity(0.5)
    ..userInteractions = false
    ..dismissOnTap = false;
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: PageLogin(),
    );
  }
}

