
import '../constants.dart';

class SizeTool {

  ///利用螢幕寬度決定字體 Size
  static double setTextSize(double screenWidth, String getType) {
    double resultValue = 0.0;
    if(screenWidth <= 600){
      switch(getType) {
        case Constants.getTitleSize :
          resultValue = Constants.mobileTitleSize;
          break;
        case Constants.getContentSize :
          resultValue = Constants.mobileContentSize;
          break;
        case Constants.getContentLargeSize :
          resultValue = Constants.mobileContentLargeSize;
          break;
      }
    } else if (screenWidth <= 1200) {
      switch(getType) {
        case Constants.getTitleSize :
          resultValue = Constants.tabletTitleSize;
          break;
        case Constants.getContentSize :
          resultValue = Constants.tabletContentSize;
          break;
        case Constants.getContentLargeSize :
          resultValue = Constants.tabletContentLargeSize;
          break;
      }
    } else {
      switch(getType) {
        case Constants.getTitleSize :
          resultValue = Constants.desktopTitleSize;
          break;
        case Constants.getContentSize :
          resultValue = Constants.desktopContentSize;
          break;
        case Constants.getContentLargeSize :
          resultValue = Constants.desktopContentLargeSize;
          break;
      }
    }

    return resultValue;
  }

  ///利用螢幕寬度決定 Icon Size
  static double setIconSize(double screenWidth) {
    if(screenWidth <= 600){
      return Constants.mobileIconSize;
    } else if (screenWidth <= 1200) {
      return Constants.tabletIconSize;
    } else {
      return Constants.desktopIconSize;
    }
  }

}