import 'package:flutter/material.dart';

ThemeData appThemeData = ThemeData(
  brightness: Brightness.light,
  primaryColor: Colors.pinkAccent,
  primarySwatch: Colors.pink,
);

ThemeData darkAppThemeData = ThemeData(
  brightness: Brightness.dark,
  primaryColor: Colors.blueGrey,
  primarySwatch: Colors.blueGrey,
);