import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:work/controller/controller_setting.dart';
import '../constants.dart';
import '../helper/size_tool.dart';

class PageSetting extends GetView<ControllerSetting> {
  const PageSetting({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: AutoSizeText(
          "setting".tr,
          style: TextStyle(
            fontSize: SizeTool.setTextSize(Get.width, Constants.getTitleSize,),
          ),
        ),
      ),
      body: _buildBody(),
    );
  }


  Widget _buildBody(){
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _languageSelectBtn(),
          _goToThemePageBtn(),
        ],
      ),
    );
  }

  ///選擇語系的按鈕
  Widget _languageSelectBtn() {
    return ElevatedButton.icon(
      onPressed: () {
        changeLanguageAlertDialog();
      },
      icon: Icon(
        Icons.language,
        size: SizeTool.setIconSize(Get.width),),
      label: AutoSizeText(
        "language".tr,
        style: TextStyle(
          fontSize: SizeTool.setTextSize(Get.width, Constants.getTitleSize,),
        ),
      ),
    );
  }

  ///變更語言的選單
  changeLanguageAlertDialog() {
    AlertDialog alert = AlertDialog(
      title: Text(
        "choose_your_language".tr,
        style: TextStyle(
          fontSize: SizeTool.setTextSize(Get.width, Constants.getTitleSize,),
        ),
      ),
      content: SizedBox(
          width: double.maxFinite,
          child: ListView.separated(
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GestureDetector(
                    child: Text(controller.locale[index]['name']),
                    onTap: () {
                      // print(locale[index]['name']);
                      controller.saveLocal(controller.locale[index]['name']);
                      controller.updateLanguage(controller.locale[index]['locale']);
                    },
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return const Divider(
                  color: Colors.blue,
                );
              },
              itemCount: controller.locale.length)),
    );
    // show the dialog
    showDialog(
      context: Get.context!,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  ///主題設定的按鈕
  Widget _goToThemePageBtn() {
    return ElevatedButton.icon(
      onPressed: () {
        controller.getTheme();
        Get.toNamed('/PageTheme');
      },
      icon: Icon(
        Icons.color_lens_outlined,
        size: SizeTool.setIconSize(Get.width),),
      label: AutoSizeText(
        "theme_setting".tr,
        style: TextStyle(
          fontSize: SizeTool.setTextSize(Get.width, Constants.getTitleSize,),
        ),
      ),
    );
  }

}
