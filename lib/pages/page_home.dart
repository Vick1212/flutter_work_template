import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:work/controller/controller_home.dart';
import 'package:work/helper/size_tool.dart';
import '../constants.dart';

class PageHome extends GetView<ControllerHome> {
  const PageHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: AutoSizeText(
          "home".tr,
          style: TextStyle(
            fontSize: SizeTool.setTextSize(Get.width, Constants.getTitleSize,),
          ),
        ),
        actions: [
          _refreshBtn(),
        ],
      ),
      body: _buildBody(),
    );
  }


  Widget _buildBody(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _nameField(),
      ],
    );
  }

  ///更新畫面的按鈕
  Widget _refreshBtn(){
    return Padding(
      padding: const EdgeInsets.only(right: Constants.padding,),
      child: InkWell(
        onTap: () {
          controller.getBasicData();
        },
        child: const Icon(Icons.refresh),
      ),
    );
  }

  ///Name欄位
  Widget _nameField(){
    return Center(
      child: SizedBox(
        height: Get.height * 0.08,
        width: Get.width * 0.88,
        child: TextFormField(
          controller: controller.nameTextController.value,
          decoration: InputDecoration(
            labelText: "name".tr,
            labelStyle: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
            filled: true,
            fillColor: Colors.blue.shade200,
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: const BorderSide(
                color: Colors.blue,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: const BorderSide(
                color: Colors.blue,
                width: 2.0,
              ),
            ),
          ),
        ),
      ),
    );
  }

}
