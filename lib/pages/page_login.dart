import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class PageLogin extends StatelessWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _loginBtn(),
        ],
      ),
    );
  }

  ///登入按鈕
  Widget _loginBtn(){
    return ElevatedButton(
      onPressed: () {
        _doSomeFakeApi();
       // Get.toNamed('/PageLanding');
      },
      child: Text("login".tr),
    );
  }

  ///模擬與Api溝通
  _doSomeFakeApi() async {
    EasyLoading.show(status: "${"loading".tr}...");
    try{
      var response = await Dio().get(
        'https://jsonplaceholder.typicode.com/users/1',
        options: Options(
          contentType: 'application/json',
        ),
      );
      EasyLoading.dismiss();
      debugPrint(response.toString());
      Get.toNamed('/PageLanding');
    }catch (e) {
      EasyLoading.dismiss();
      debugPrint(e.toString());
    }

  }




}
