import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:work/controller/controller_setting.dart';
import '../../constants.dart';
import '../../helper/size_tool.dart';

class PageTheme extends GetView<ControllerSetting> {
  const PageTheme({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: AutoSizeText(
            "theme_setting".tr,
            style: TextStyle(
              fontSize: SizeTool.setTextSize(Get.width, Constants.getTitleSize,),
            ),
          ),
        ),
        body: _buildBody(),
      );
    });
  }

  Widget _buildBody(){
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _themeChangeSwitch(),
        ],
      ),
    );
  }

  ///變更主題的按鈕
  Widget _themeChangeSwitch(){
    List<List<Color>> twoColor = [[Colors.pink, Colors.pink.shade200], [Colors.black87, Colors.blueGrey]];
    List<String> displayItemList = [("light".tr), ("dark".tr)];
    return ToggleSwitch(
      minWidth: Get.width * 0.8 * (1 / controller.itemList.length),
      minHeight: Get.height * 0.1,
      initialLabelIndex: controller.themeSelectIndex.value,
      cornerRadius: 10.0,
      activeBgColors: twoColor,
      activeFgColor: Colors.white,
      inactiveBgColor: Colors.grey,
      inactiveFgColor: Colors.white,
      totalSwitches: controller.itemList.length,
      animationDuration: 300,
      labels: displayItemList,
      fontSize: 18,
      animate: true, // with just animate set to true, default curve = Curves.easeIn
      curve: Curves.easeInCirc, // animate must be set to true when using custom curve
      onToggle: (index) {
        controller.changeTheme(index!);
      },
    );
  }

}
