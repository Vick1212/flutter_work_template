import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:work/theme/theme_constants.dart';
import '../constants.dart';


class ControllerSetting extends GetxController {

  late Worker _ever;

  final List locale = [
    {'name': 'ENGLISH', 'locale': const Locale('en', 'US')},
    {'name': '中文', 'locale': const Locale('zh', 'TW')},
  ];
  List<Icon> itemList = [const Icon(Icons.light_mode), const Icon(Icons.dark_mode)];
  var themeSelectIndex = 0.obs;
  Locale? savedLocalLanguage;

  @override
  onInit() {
    super.onInit();
    EasyLoading.init();
    EasyLoading.addStatusCallback((status) {
      debugPrint('EasyLoading Status $status');
    });
    configLoading();
  }

  disposeWorker() {
    _ever.dispose();
    // or _ever();
  }

  void configLoading() {
    EasyLoading.instance
      ..displayDuration = const Duration(milliseconds: 2000)
      ..indicatorType = EasyLoadingIndicatorType.cubeGrid
      ..loadingStyle = EasyLoadingStyle.dark
      ..indicatorSize = 45.0
      ..radius = 10.0
      ..progressColor = Colors.yellow
      ..backgroundColor = Colors.green
      ..indicatorColor = Colors.yellow
      ..textColor = Colors.yellow
      ..maskColor = Colors.blue.withOpacity(0.5)
      ..userInteractions = false
      ..dismissOnTap = false;
  }



  ///儲存語系選項
  saveLocal(String local) async {
    final prefs = await SharedPreferences.getInstance();
    switch(local){
      case "ENGLISH":
        await prefs.setString('lang', "en");
        break;
      case "中文":
        await prefs.setString('lang', "zh");
        break;
    }
  }

  ///更新語系
  updateLanguage(Locale locale) {
    Get.back();
    Get.updateLocale(locale);
  }

  ///取得儲存的語系設定(如果有的話)
  Future<Locale> getLocal() async {
    final prefs = await SharedPreferences.getInstance();
    final String? language = prefs.getString('lang');
    Locale savedLocal = const Locale("en", "US");
    if (language != null) {
      switch (language) {
        case "en":
          savedLocalLanguage = const Locale("en", "US");
          savedLocal = const Locale("en", "US");
          break;
        case "zh":
          savedLocalLanguage = const Locale("zh", "TW");
          savedLocal = const Locale("zh", "TW");
          break;
      }
    }
    return savedLocal;
  }

  ///變更主題色調
  changeTheme(int themeIndex){
    switch(themeIndex){
      case 0 :
        Get.changeTheme(appThemeData);
        Get.changeThemeMode(ThemeMode.light);
        saveTheme(Constants.themeLight);
        break;
      case 1 :
        Get.changeTheme(darkAppThemeData);
        Get.changeThemeMode(ThemeMode.dark);
        saveTheme(Constants.themeDark);
        break;
    }
  }

  ///儲存Theme
  saveTheme(String themeType) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString("themeType", themeType);
  }

  ///取得當前儲存的Theme
  Future<ThemeData> getTheme() async {
    final prefs = await SharedPreferences.getInstance();
    final String? customTheme = prefs.getString('themeType');
    if (customTheme != null) {
      switch (customTheme) {
        case Constants.themeLight:
          themeSelectIndex.value = 0;
          break;
        case Constants.themeDark:
          themeSelectIndex.value = 1;
          break;
      }
    }
    return themeSelectIndex.value == 0 ? appThemeData : darkAppThemeData;
  }

}
