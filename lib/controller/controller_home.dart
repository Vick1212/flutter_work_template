import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class ControllerHome extends GetxController {

  late Worker _ever;

  Rx<TextEditingController> nameTextController = TextEditingController().obs;


  @override
  onInit() {
    super.onInit();
    EasyLoading.init();
    EasyLoading.addStatusCallback((status) {
      debugPrint('EasyLoading Status $status');
    });
    configLoading();
  }

  void configLoading() {
    EasyLoading.instance
      ..displayDuration = const Duration(milliseconds: 2000)
      ..indicatorType = EasyLoadingIndicatorType.cubeGrid
      ..loadingStyle = EasyLoadingStyle.dark
      ..indicatorSize = 45.0
      ..radius = 10.0
      ..progressColor = Colors.yellow
      ..backgroundColor = Colors.green
      ..indicatorColor = Colors.yellow
      ..textColor = Colors.yellow
      ..maskColor = Colors.blue.withOpacity(0.5)
      ..userInteractions = false
      ..dismissOnTap = false;
  }

  disposeWorker() {
    _ever.dispose();
    // or _ever();
  }

  ///取得基本資料
  getBasicData(){
    _getUserName();
  }

  ///取得使用者名稱 (模擬)
  _getUserName(){
    EasyLoading.show(status: "${"loading".tr}...");
    Future.delayed(const Duration(seconds: 2), () {
      nameTextController.value.text = "John";
      EasyLoading.dismiss();
    });
  }


}
