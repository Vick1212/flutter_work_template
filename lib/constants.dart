
class Constants{

  static const double padding = 20.0;


  static const double fontSizeForMobileTitle = 24.0;
  static const double fontSizeForMobileContent = 16.0;
  static const double fontSizeForTabletTitle = 48.0;
  static const double fontSizeForTabletContent = 32.0;

  static const String getTitleSize = "getTitleSize";
  static const String getContentSize = "getContentSize";
  static const String getContentLargeSize = "getContentLargeSize";
  static const double mobileTitleSize = 24;
  static const double mobileContentSize = 18;
  static const double mobileContentLargeSize = 20;
  static const double tabletTitleSize = 48;
  static const double tabletContentSize = 36;
  static const double tabletContentLargeSize = 40;
  static const double desktopTitleSize = 72;
  static const double desktopContentSize = 54;
  static const double desktopContentLargeSize = 60;
  static const double mobileIconSize = 36;
  static const double tabletIconSize = 64;
  static const double desktopIconSize = 96;
  static const double mobileIconSizeLarge = 42;
  static const double tabletIconSizeLarge = 63;

  static const String themeLight = "themeLight";
  static const String themeDark = "themeDark";

}