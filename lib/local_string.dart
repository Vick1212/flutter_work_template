import 'package:get/get.dart';

class LocaleString extends Translations {
  @override
  Map<String, Map<String, String>> get keys =>
      {
        'en_US': {
          "home": "Home",
          "setting": "Setting",
          "language": "Language",
          "choose_your_language": "Choose your language",
          "name": "Name",
          "loading": "Loading",
          "theme_setting": "Theme setting",
          "light": "Light",
          "dark": "Dark",
          "login": "Login",
        },
        'zh_TW': {
          'home': '首頁',
          "setting": "設定",
          "language": "語言",
          "choose_your_language": "選擇語言",
          "name": "名字",
          "loading": "讀取中",
          "theme_setting": "主題設定",
          "light": "明亮",
          "dark": "深色",
          "login": "登入",
        }
      };
}