import 'package:get/get.dart';
import 'package:work/pages/page_home.dart';
import 'package:work/pages/page_landing.dart';
import 'package:work/pages/page_setting.dart';
import 'package:work/pages/setting_children/page_theme.dart';

class AppPages {
  AppPages._();

  static final routes = [
    GetPage(
      name: '/PageLanding',
      page: () => const PageLanding(),
      transition: Transition.circularReveal,
      transitionDuration: const Duration(milliseconds: 500),
    ),
    GetPage(
      name: '/PageHome',
      page: () => const PageHome(),
      transition: Transition.circularReveal,
      transitionDuration: const Duration(milliseconds: 500),
    ),
    GetPage(
      name: '/PageSetting',
      page: () => const PageSetting(),
      transition: Transition.circularReveal,
      transitionDuration: const Duration(milliseconds: 500),
    ),
    GetPage(
      name: '/PageTheme',
      page: () => const PageTheme(),
      transition: Transition.circularReveal,
      transitionDuration: const Duration(milliseconds: 500),
    ),

  ];
}